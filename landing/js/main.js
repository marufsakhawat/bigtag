(function ($) {
"use strict";

    
    // niceSelect
    $('select').niceSelect();
    
    // owlCarousel
    $('.brand-logo-carousel').owlCarousel({
        loop: true,
        margin: 30,
        items: 4,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 3
            },
            992: {
                items: 4
            },
            1600: {
                items: 6
            }
        }
    });
    
    // slick carousel
    $('.from-blog-carousel').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        centerMode: false,
        focusOnSelect: false,
        arrows: false,
        vertical: true,
        verticalSwiping: true,
        dots: true,
    });    
    
    // meanmenu
    $('#mobile-menu').meanmenu({
    	meanMenuContainer: '.mobile-menu',
    	meanScreenWidth: "992"
    });

    // One Page Nav
    var top_offset = $('.header-area').height() - 10;
    $('.main-menu nav ul').onePageNav({
    	currentClass: 'active',
    	scrollOffset: top_offset,
    });


    $(window).on('scroll', function () {
    	var scroll = $(window).scrollTop();
    	if (scroll < 245) {
    		$(".header-sticky").removeClass("sticky");
    	} else {
    		$(".header-sticky").addClass("sticky");
    	}
    });


    // magnificPopup img view
    $('.popup-image').magnificPopup({
    	type: 'image',
    	gallery: {
    	  enabled: true
    	}
    });

    /* magnificPopup video view */
    $('.popup-video').magnificPopup({
    	type: 'iframe'
    });


    // isotop
    $('.grid').imagesLoaded( function() {
    	// init Isotope
    	var $grid = $('.grid').isotope({
    	  itemSelector: '.grid-item',
    	  percentPosition: true,
    	  masonry: {
    		// use outer width of grid-sizer for columnWidth
    		columnWidth: '.grid-item',
    	  }
    	});
    });

    // filter items on button click
    $('.portfolio-menu').on( 'click', 'button', function() {
      var filterValue = $(this).attr('data-filter');
      $grid.isotope({ filter: filterValue });
    });

    //for menu active class
    $('.portfolio-menu button').on('click', function(event) {
    	$(this).siblings('.active').removeClass('active');
    	$(this).addClass('active');
    	event.preventDefault();
    });




    // scrollToTop
    $.scrollUp({
    	scrollName: 'scrollUp', // Element ID
    	topDistance: '300', // Distance from top before showing element (px)
    	topSpeed: 300, // Speed back to top (ms)
    	animation: 'fade', // Fade, slide, none
    	animationInSpeed: 200, // Animation in speed (ms)
    	animationOutSpeed: 200, // Animation out speed (ms)
    	scrollText: '<i class="fa fa-sort-up"></i>', // Text for element
    	activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });

    // WOW active
    new WOW().init();


})(jQuery);